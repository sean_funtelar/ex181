﻿using System;

namespace week3_ex18
{
    class Program
    {
        static void Main(string[] args)
        {
            var num1 = 5;
            var num2 = 4;
            var num3 = 6;
            var num4 = 3;
            var num5 = 2;
            var num6 = 7;
            Console.WriteLine($"A) The value of num4 = {num4}");
            Console.WriteLine($"B){num1} + {num2} = {num1+num2}");
            var num7 = num3+num6+num5;
            Console.WriteLine($"C){num7}={num3}+{num6}");
            var think = "I am thinking...";
            Console.WriteLine($"{think + num3}");
        }
    }
}